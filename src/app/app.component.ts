import { Component } from '@angular/core';

@Component({
  selector: 'pruebas-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private urls = {
    perretes: 'http://patasbox.es/wp-content/uploads/2016/09/pug-rain-coat.jpg',
    gatetes: 'https://pbs.twimg.com/media/D12gXVyWkAAQ8rL.jpg'
  };
  public tipoAnimal = 'gatetes';
  public urlImagen = this.urls[this.tipoAnimal];
  public fotoBonita = false;
  public radioEsquinas = 0;

  constructor() {

  }

  private cambiaAnimal(e: Event) {
    e.preventDefault();
    this.tipoAnimal = 'perretes';
    this.urlImagen = this.urls[this.tipoAnimal];
    this.fotoBonita = true;
    this.radioEsquinas = 500;
  }

  public getClasesFoto() {
    return {
      borde: this.fotoBonita,
      small: !this.fotoBonita
    };
  }
}
